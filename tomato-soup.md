# Tomato Basil Soup

**keywords**: vegan, soup

## Ingredients

- 1 tablespoon olive oil or 1/4 cup water
- 1 large white onion, sliced
- 6 cloves garlic, minced
- 1 kg tomatoes (roma, plum or tomatoes on the vine)
- 1/2 cup basil leaves (about 10 –12 large leaves) or 2 teaspoons dried
- 1/2 teaspoon dried oregano
- 2 cups low-sodium vegetable broth
- salt + pepper, to taste
- vegan cream, to serve


## Instructions

1. **Prep**: Start by prepping the onion and garlic. Cut the quarter the tomatoes or cut into 1 inch pieces if using larger tomatoes.
1. **Saute**: In a heavy bottom pan, heat oil or water over medium heat, saute the onion for 7 minutes, add the garlic and cook 1 minute more.
1. **Add remaining ingredients**: Add the tomatoes, basil, oregano, pinch of salt and pepper, and broth.
1. **Simmer**: Bring the chunky soup to a boil, cover, reduce heat and simmer for 15 minutes.
1. **Puree**: Once soup is done, let rest for 10 minutes to cool. Using an immersion blender or cup blender, puree the soup until desired consistency. Season with more salt & pepper as needed. If soup seems bland, it needs more salt. Stir in a couple tablespoons of vegan cream if you like.
1. **Serve**: Serve with a drizzle of vegan cream, basil and fresh cracked pepper. For a little heat, add a pinch of red pepper flakes. Pair with homemade Artisan Bread or soft and chewy Vegan Naan for soaking up the juices.
1. **Store**: Leftovers can be stored in the refrigerator for up to 6 days in a covered container. To store longer, freeze for up to 2 – 3 months.

## Notes

If you don’t have good fresh tomatoes, sub with 2 cans (28oz.) cans whole peeled tomatoes, juices and all.

Other thickening ideas: Add 1 – 2 tablespoons tahini or cashew butter to the pot. Add more pureed tomatoes.