# Spicy Zucchini Soup

## Ingredients

- 4 tablespoons extra virgin olive oil
- 1 onion, finely chopped
- 1/2 jalapeño chile (or more to taste, depending on how spicy you would like the soup to be), seeds, stems and ribs removed and discarded, chopped
- 3 chopped garlic cloves
- 2 pounds chopped zucchini (skin on), about 5-6 cups
- 1 1/2 cups chopped day-old bread (or 1 cup rice or cooked or uncooked potatoes)
- 3 cups chicken broth (use vegetable broth for vegetarian or vegan option)
- 1 cup water
- 1/2 cup fresh mint leaves (spearmint), loosely packed, chopped
- 1/2 cup fresh cilantro, loosely packed, chopped (optional)
- 2 teaspoons lemon juice
- Salt, to taste
- Pepper, to taste
- Lemon wedges (for garnish), optional

## Method

1. Sauté the onions (1 onion) and jalapeño (1/2 jalapeno): Heat the olive oil (4 tbsp) in a large pot over medium-high heat. Add the onion and the chopped jalapeño, and sauté for 4 to 5 minutes until the onions are translucent, but not browned.
1. Cook the garlic (3 cloves) and zucchini (5-6 cups, 900 g). Add the garlic and zucchini to the pan and sauté for another 3 to 4 minutes, stirring often. Sprinkle with salt.
1. Add the thikener (1 1/2 bread or 1 cup rice or 1 cup potatoes) and liquids (3 cups broth + 1 cup water): Add the bread, broth, and water, and bring to a simmer. Reduce the heat to low and simmer gently for 20 minutes. (Note: If you're using uncooked potatoes instead, you may need to simmer a little longer, depending on what kind of potatoes you use.)
1. Purée the soup: Remove the soup from the heat. Add the mint (1/2 cup) and cilantro (1/2 cup) (if using). Purée in a blender or food processor until smooth, working in batches if necessary. (You can also use an immersion blender.)
1. Add lemon juice (2 tsp), salt, pepper (to taste): Return the soup to the pot. Add the lemon juice, along with salt and pepper to taste.
1. Garnish and serve: Garnish with lemon wedges and sprigs of mint or cilantro. Serve hot or chilled. Keeps for up to a week in the fridge.
