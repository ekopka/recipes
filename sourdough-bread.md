# Sourdough Bread

## Ingrediants

### 1 Loaf

1. 520g flour
1. 10g salt
1. 90g starter
1. 385g water

### 2 Loaves

1. 1040g flour
1. 20g salt
1. 180g starter
1. 770g water

### 1.5 Loaf

1. 780g flour
1. 15g salt
1. 135g starter
1. 577.5g water


The flour needs to be strong flour or bakers flour. Buying it in bulk is often the best option.

## Starter

Feed the starter every 4-5 days.

Feed with flour and water. Use the flour for the type of starter it is. Use rye flour for rye starter and bakers flour for white starter. Generally the ratio is 1 heaped tablespoon with 1 tablespoon of water. Feed the starter with 2 tablespoons of flour, more flour can be used if you need more starter sooner. Try to get the consistency of the starter to be like a thick paste, like ketchup.

Always feed the starter either 8 hours before making bread or directly after making bread.

## Method

### Step 1: Prepare the dough

Remove the starter from the fridge a few hours before making the dough. This helps to acitivate the starter, which is when you see many bubbles in the starter. If you forget to remove the starter earlier you can still make the dough, it just might take a bit longer to rise.

Measure out the dry ingredients (flour and salt) into a bowl and mix them together. When making rye or wholemeal bread use about 1/4 of the total flour as rye/wholemeal and for the rest use white flour. You can add additional ingredients such as sunflower or lin seeds to the flour mix to make a grainy bread.

In a seperate bowl measure out the starter and water. Whisk them together, ensuring that the starter is well combined with the water, there should be no solid bits of starter.

When measuring these ingredients ensure you are as accurate as possible.

Combine the starter mixture with the flour mixture, mix them together to form the dough. You can use a spoon initially to mix the water in but once the dough form the easiest method is to use your hand to mix the dough completely.

### Step 2: Rising

Cover the bowl with a lid. If your bowl does not have a lid you can use a damp tea towel. The dough will need to rise for at least 12 hours. For the first hour, every 15 minutes, turn/fold the dough. This can be done by grabbing the side of the dough, lifting it up and folding it ontop of itself. Do this fully around the circumference of the dough. To prevent your fingers from sticking to the dough wet your hand with some cold water.

Leave the dough at room temperature to rise for 12 hours. This can be done overnight. The dough has finished rising when it has doubled in size. The exact timing can vary depending on the weather and especially the temperature. The dough doubling in size is a good indication that it is ready. If you see bubbles forming on the surface of the dough then it is definelty ready and you should immedietely begin the next step.

### Step 3: Prepare the proofing container

Before working on the dough I like to prepare the proofing container for the dough. You can use a proofing basket for this step but I generally use the loaf tin that the bread will be cooked in. I prepare the tin with baking paper and a light spray of cooking oil. Also prepare a damp tea towel.

### Step 4: Pressing out the air

Take the dough out of the bowl onto a surface lightly powdered with flour. If you made a double lot, divide the dough into two separate loaves now. Press the air out of the dough by grabbing the dough from the outside, lifting it up and pressing it into the middle. Use either your fingers or the base of your thumb. Rotate the dough and continue pressing out the air until it is done. This usually only takes a couple of minutes. While working with the dough you can hear the air being pressed out if you listen closely, and sometimes even see the large bubbles coming to the surface. You know you are done when you no longer hear any air being released.

### Step 5: Shape the dough

Flip the dough over and use your palms to pull the outside surface of the dough and press it underneath the dough. The goal here is to shape the dough into a bread shape. Repeat this a few times, while also rotating the dough, until you form a nice loaf.

### Step 6: Proof

Place the loaf into the prepared proofing container. Sprinkle the dough with flower, to prevent it sticking to the tea towel, and cover the container with a damp tea towel. Place the dough in the fridg. The dough must be left in the fridge for at least 1 hour but can remain in the fridge for up to 24 hours. This can be useful if you want to bake the bread later. 

### Step 7: Bake

Prepare the oven 40 minutes in advance. Preheat to 250 C. Place an old baking tray half filled with water on the bottom shelf of the oven.

Take the loaf from the fridge and use a knife to score the bread. Make some cuts on the surface of the dough, this is so that the bread will be able to expand as it rises in the oven.

Place the bread in the top shelf of the oven, ensure that it is not directly over the baking tray filled with water.

Bake at 250 C (230 C fan forced) for 20 minutes.

Bake at 220 C (200 C fan forced) for 25 minutes.

Take the bread out of the oven and let it rest for 1 hour before serving.
