# Vegan Vegetable Lasagna

## Ingeredients

- 1 tablespoon olive oil or ¼ cup water (for water saute)
- 1 small onion, diced
- 2 – 3 cloves garlic, minced
- 2 carrots (2 cups), diced
- 1 zucchini (2 cups), diced
- 1 yellow squash (2 cups), diced
- 220 g mushrooms, chopped
- ½ teaspoon Italian seasoning (or thyme, basil or marjoram), optional
- 1 package (~300g) frozen spinach, thawed and drained
- 2 cups vegan ricotta (Cashew Ricotta Cheese or Tofu Ricotta)
- 1 jar (~700g) pasta sauce, about 3 cups
- 9 lasagna noodles (approx.), regular or no-boil
- salt & pepper, to taste


## Method

### Prepare

1. Make [vegan cashew ricotta](vegan-cashew-ricotta.md)
1. Cook noodles if needed
1. Preheat Oven to 170 C


### Vegetables

Prepare First:
- 1 small onion, diced
- 2 – 3 cloves garlic, minced

Then Prepare:
- 2 carrots (2 cups), diced
- 1 zucchini (2 cups), diced
- 1 yellow squash (2 cups), diced
- 220 g mushrooms, chopped

Could experiment with vegatables

1. Saute onions and garlic in a large pan for 5 minutes.
1. Add remaining vegetables (carrots, zuchini, squash, mushrooms)
1. Add herbs (1/2 tsp italian seasoning or thyme and basil), salt and pepper to taste
1. Saute for 7 minutes.
1. Remove from heat.

Note: We don’t need to saute the zucchini, yellow squash and mushrooms too much, we want their juices to fully release while baking.

### Ricotta
Mix together the cashew ricotta (2 cups) and spinach (300g), mix well.

### Assemble

Use a large rectangular baking dish.

Layers:
| | |
|-|-|
| top | 1/3 of sauce |
|  | pasta  |
|  | 1/2 of vegetables |
|  | 1/2 of ricotta |
|  | pasta |
|  | 1/3 of sauce |
|  | 1/2 of vegetables |
|  | 1/2 of ricotta |
|  | pasta |
| bottom | 1/3 of sauce |

Cover with lid or foil

### Bake

1. Cover and bake on the center rack for 40 minutes. 
1. Let rest covered for 5 minutes, remove cover and let cool 10 minutes. 
1. If not using no-boil noodles, you can remove the cover half way through for a baked look.


### Notes

Leftovers can be stored in the refrigerator for up to 5 – 6 days, in a covered container.

They veggies are more of a ‘summer’ variety, but you easily change them up using fall produce.

If you feel you want extra protein, add a can of white beans (drained and rinsed) or a package of fresh or thawed frozen peas in between the ricotta and vegetables.

If you don’t care for mushrooms, sub in a diced, large red bell pepper.

Make ahead and freeze: Simply prepare the lasagna ahead of time (up until baking), and keep it stored in the freezer for up to 2 – 3 months. When ready to bake, preheat oven to 190 C, bake covered for 45 minutes.

Reheat: Individual slices can be warmed in the microwave. To reheat larger portions, preheat the oven to 220 C. Add between 2 – 4 tablespoons of water over the lasagna (depending on its size) and cover. Alternatively, add ¼ cup marinara sauce to prevent dryness. Bake for 25 minutes, or until the lasagna is hot and bubbly.


Source: https://simple-veganista.com/the-ultimate-vegetable-lasagna/
