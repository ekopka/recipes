# Hummingbird Cake

## Ingredients
### Cream cheese icing

- 250 gram cream cheese, at room temperature
- 100 gram butter, chopped, at room temperature
- 1 1/2 cup icing sugar, sifted
- 1 tablespoon pineapple juice
- 1 teaspoon lemon zest, finely grated


### Hummingbird cake

- 1 3/4 cup self-raising flour
- 1 cup caster sugar
- 1/2 cup desiccated coconut
- 1/2 cup walnuts, finely chopped
- 1 teaspoon bicarbonate of soda
- 440 gram can crushed pineapple, drained
- 2 large over-ripe bananas, mashed
- 2 eggs, lightly whisked
- 3/4 cup vegetable or sunflower oil
- 1/2 quantity cream cheese icing
- banana chips and flaked coconut, to decorate


## Method
### Cream cheese icing

1. Using an electric mixer, beat cream cheese and butter in a small bowl until light and creamy.
1. Add half the sugar, juice and zest; beat until combined. Add remaining sugar; beat until light and fluffy.


### Hummingbird cake

1. Preheat oven to 180°C/160°C fan forced. Grease and line the base and sides of a 21x10cm (base measurement) loaf pan with baking paper, extending paper at long sides for handles.
1. Combine flour, sugar, coconut, walnuts and bicarbonate of soda in a large bowl. Make a well at centre.
1. Combine pineapple, banana, egg and oil in a jug. Add pineapple mixture to flour mixture; stir to combine.
1. Spoon into prepared pan; level surface. Bake for 1 hour or until skewer inserted at centre comes out clean. Cool in pan for 5 minutes. Transfer to a wire rack to cool completely. Transfer cake to serving plate.
1. Make Cream Cheese icing as recipe directs. Spread icing over top of cooled cake. Decorate with banana chips and flaked coconut. Cut into slices. Serve.


[source](https://www.womensweeklyfood.com.au/recipes/hummingbird-cake-21179)
