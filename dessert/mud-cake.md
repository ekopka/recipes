# Vegan Chocolate Mudcake

## Ingredients

- 200g Wholemeal flour
- 200g Plain flour
- 4 teaspoons baking powder
- 60g Cocoa
- 350g Caster sugar
- 1 teaspoon vanilla essence
- 250ml Vegetable oil
- 350ml Water

## Directions

1. Preheat oven to 160 degrees C.
1. Mix all dry ingredients together.
1. Add wet ingredients and mix with a mixer on high for a couple of minutes.
1. Pour into baking tin (or try 2 round cake tins and ice in the middle and on top).
1. Cook for approx 40mins or until a skewer comes out clean.
1. Allow to cool to room temperature, then ice with vegan choc icing or dust with icing sugar.
