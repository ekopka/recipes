# Hazelnut Brownies

## Ingredients

- 200 grams chocolate chips ( a mixture of unsweetened and semi-sweet chocolate chips)
- 3/4 cup (170 grams) butter, roughly chopped
- 4 eggs
- 1 cup (200 grams) sugar
- 1 teaspoon vanilla extract
- 3/4 cup (84 grams) hazelnut meal
- 1/2 cup semi-sweet chocolate chips


## Instructions

1. In a heatproof bowl, add the butter and 7 oz chocolate chips. Microwave for 1 minute and stir to combine. Continue to microwave, stopping every 15 seconds until the chocolate and butter are combined, smooth and silky.
1. Set aside to cool slightly.
1. Heat the oven to 180C. Line a 9X9" baking pan with parchment paper.
1. In a mixing bow, add the eggs and the sugar. With the paddle attachment on, beat on low to medium speed for 8 to 10 minutes until the mixture is thick, pale and almost tripled in volume.
1. Pour in the melted chocolate and vanilla extract.
1. With a spatula, gently fold in the hazelnut meal and the remaining 1/2 cup chocolate chips.
1. Pour the batter into the prepared pan and bake in preheated over for 40 to 45 minutes.
1. Remove from the oven and let cool completely before cutting into bars.
