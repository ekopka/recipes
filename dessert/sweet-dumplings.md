# Sweet Dumplings

How to make sweet dumplings

1. You can use any nuts you like peanuts, sesame,walnuts. 

2. Crush them into small pieces, mix with a bit of sugar, some rice oil or any oil you like.

3. Cook the mix on a frying pan, untill you can smell the fragrent of the nuts, cool them down to room temp.

4. Use boiled water as hot as you can take, mix the glutinous rice flour with chopsticks, little by little add the hot water into the rice flour untill you can roll it into a dough, not too sticky with your hands would be the ready stage to make the dumplings.

5. You can use a rolling pin to make a big flat skin then use a cookie cutter to cut into small pieces around 6-8cm diameter skin then put some the nuts mixture in the centre of the skin, curling it up and seal the dumping by pinch the skin together. (if a lot of skin on top just nip it off and put it back to rest of the dough to make more skin later)(Do not put too much nuts mix on the skin first just in case can’t seal the dumpling...)

6. Roll the rice dumpling into a round ball then boil a pot of water, put the dumplings into it after the water is boiled, stir them once putting into water, stir from the bottom of the pot, make sure they are not sticked to the pot, once dumplings are afloating, cover the lid to the pot, stay with the pot in case of overflowing the water, if its a big dumplings it may need about 5 to 8 minutes to cook. (Timing starts when the water is boiling again after the dumpling is in the pot. )

7. Let them cool down then enjoy the sweet dumpling!