# Magwinya

Prepare the dough
- Combine dry ingredients.
    - 4 cups flour
    - 1 teaspoon yeast
    - pinch of salt
    - 1/3 cup sugar (any kind)
- Add warm water (about 1 large mug) and mix to form a dough.
- Kneed dough to right consistency, when no powdered flour remains, use extra flour as necessary.
- Let the dough sit for 30 minutes.

Frying the Magwinya

- Heat 2 cm deep of oil to high temperature.
- Use oil or flour to avoid dough sticking to hands.
- Form portions of dough into the desired shape.
- Cook for a few minutes until red/brown.
- Then flip to cook on the other side.
- Remove from oil and place on paper towel.


Cook in steam for bread dumplings to eat with veggies.
