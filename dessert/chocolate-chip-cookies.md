# Cadbury Chocolate Chip Cookies

## Ingredients

- 125g butter, softened
- 1/2 cup brown sugar
- 1/3 cup caster sugar
- 1 egg
- 1 teaspoon vanilla
- 1 + 1/2 cups self raising flour
- 3/4 cup CADBURY Baking Milk Chocolate Chips 

## Method

1. CREAM together butter and sugars until light and fluffy. Whisk in egg and vanilla. Stir in flour then fold through Chocolate Chips.
2. PLACE teaspoonsful on greased baking trays. Bake in a moderately slow oven 160°C for 10-15 minutes or until cooked. Cool on a wire rack. 
