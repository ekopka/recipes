# Ania’s Muffins

## Ingredients
- 1 ¾ cup self raising flour
- 1 ½ tsp mixed spice
- ½ cup brown sugar
- fruit or choc chip
- 1 cup milk
- ¼ cup oil
- 1 egg
- 2 tsp vanilla essence

## Method

- Pre-heat oven to 200oC.
- Mix dry ingredients together and slush fluids in a mug.
- Add them to the dry ingredients and mix well.
- Add fruit.
- Bake for 20-25 minutes.
