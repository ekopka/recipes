# Lidia’s Banana Bread

- 2 eggs
- 1 cup sugar
- 2 ripe bananas
- 2 cups self raising flower
- half a cup milk
- half a cup oil
- optional raisings, diced apples, nuts.


Method
1. In a bowl mix 2 eggs with 1 cup (or less) of sugar.
1. On a plate smash 2 ripe bananas. Add to egg mixture.
1. Add 2 cups of self raising flour, half a cup milk, half a cup of oil to mixture.
1. Optional, add raisins, diced apples, and/or nuts.
1. Bake for 40 minutes at 200 degrees
