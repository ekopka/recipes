# Greek Quinoa Salad

## Ingredients

- 1 cup dried quinoa, rinsed
- 1 3/4 cups water
- 1 teaspoon garlic powder
- 1 can (14 oz) chickpeas (garbanzo beans), drained and rinsed
- 1 cup grape tomatoes, sliced in half (diced red bell pepper would work here too)
- 1 cup English cucumber, diced
- 1/2 red onion, diced
- 1 jar (7 oz) kalamata olives (about 1 cup), pitted and sliced
- 1/4 cup loosely packed fresh parsley, chopped
- 3 tablespoons fresh dill, chopped
- mineral salt & fresh cracked pepper, to taste
- juice of 1 large lemon
- drizzle of extra virgin olive oil (optional)
- peperoncini, to serve
- lemon slices, to serve (optional)
- arugula, to serve (optional)


## Instructions

**Quinoa**: Rinse the quinoa using a fine mesh strainer. In a medium size pot, add the water, quinoa and garlic powder and bring to a boil. Cover, reduce heat and simmer for 15 minutes. Remove from heat, uncover and let stand for 10 – 15 minutes. Fluff with a fork. Alternatively, you can make this Instant Pot Quinoa.

**Assemble**: Once quinoa is ready, add in the chickpeas, tomatoes, cucumber, onion, olives and parsley. Mix well. Season with salt, fresh cracked pepper and the juice of 1 large lemon.

Serve on a bed of arugula or as is. Tastes great with pepperoncinis and a little extra lemon juice over top. Add extra virgin olive oil if you wish. Season to taste with salt + pepper.

This dish can be served warm, at room temperature or chilled.

Serves 4 – 6

**Store**: Leftovers can be stored in the refrigerator in a covered container for up to 5 – 6 days.
