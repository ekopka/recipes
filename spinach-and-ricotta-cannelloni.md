# Spinach and Ricotta Cannelloni

## Ingredients

### Sauce (you need LOTS!):
- 2 tbsp olive oil
- 1 onion , finely chopped
- 4 garlic clove , minced
- 1 bay leaf , fresh (sub dried)
- 1/2 tsp each dried thyme and oregano
- 1/3 cup (120g) tomato paste
- 800 g / 28 oz canned crushed tomato
- 4 cups (1 L) vegetable or chicken stock/broth , low sodium
- 1/3 cup (85 mL) Chardonnay or other dry white wine (sub more stock)
- 3/4 tsp salt
- 1 1/2 tsp sugar
- 1/3 tsp black pepper
- 2/3 cup (10g) basil leaves , roughly torn (recommended but not critical)

### Filling:
- 250 g / 8 oz frozen chopped spinach , thawed (Note 1)
- 500 g / 1 lb ricotta , full fat please (Note 2)
- 1/3 cup (30g) parmesan , finely shredded
- 1 cup (100g) shredded cheese (Mozzarella, Colby, Cheddar, Tasty, Gruyere, Swiss)
- 1 egg
- 1 large garlic clove , minced
- Grated fresh nutmeg (just a sprinkling) or 1/8 tsp nutmeg powder (optional)
- 3/4 tsp cooking / kosher salt
- 1/2 tsp black pepper

### Cannelloni
- 18 - 22 dried cannelloni tubes (250g pack) or 14 manicotti tubes (8.8 oz) (Note 3)
- 1/3 cup (30g) parmesan , finely shredded
- 1 1/4 cups (125g) shredded Mozzarella
- More basil and parmesan , for garnish (optional)


## Instructions

### Sauce:
1. Sauté – Heat oil in a small pot over medium high heat. Add garlic, onion, bay leaf, thyme and oregano. Cook for 3 – 4 minutes until the onion is translucent. Add tomato paste and cook for 1 minute.
1. Reduce wine – Add wine, increase heat to high and let it simmer rapidly until mostly evaporated (about 2 minutes).
1. Simmer – Add tomatoes, stock, sugar, salt and pepper. Stir then simmer on medium low for 20 minutes.
1. Blitz – Remove bay leaf then blitz with a stick blender until smooth. Simmer for 1 more minute then remove from the stove
1. Basil – Stir in basil then cover to keep warm until required.

### Filling:
1. Squeeze spinach – Place spinach in a colander and press out most of the liquid (don't need to thoroughly squeeze dry).
1. Mix filling – Place Spinach in bowl with remaining Filling ingredients. Mix, taste, adjust salt and pepper to taste (different cheeses have different saltiness).

### Assemble & Bake:
1. Preheat oven to 180°C/350°F (160°C fan).
1. Pan – Choose a baking pan which will comfortably fit about 20 cannelloni – mine is 21 x 26 cm / 8.5 x 10.5".
1. Smear – Spread 1 cup of Sauce on the base (stops cannelloni from sliding around).
1. Fill tubes – Transfer Filling to a piping bag with a large nozzle (that fits in the tubes), or use a strong ziplock bag. Or do this step using a knife (it's a bit tedious though!). Pipe the filling into the tubes.
1. Assemble – Place cannelloni in baking dish. Pour over remaining Sauce, covering all the tubes.
1. Bake covered (but naked) – Cover with foil, then bake for 30 minutes.
1. Cheese it! Remove foil, scatter over parmesan then mozzarella. Return to oven for 15 minutes until cheese is melted.
1. Serve, garnished with extra parmesan and basil if desired.

## Recipe Notes:

1. **Spinach** – I use frozen spinach for the convenience and also because I’m a sucker for the whole “snap frozen” thing. To use fresh, use about 500g/1 lb sliced spinach leaves or baby spinach leaves, saute with a little oil to wilt down and remove excess liquid. Cool then proceed with recipe.
2. **Ricotta** – Low fat ricotta is harder and drier, so it’s more difficult to pipe into the tubes plus once baked, is not as juicy and moist. Avoid Perfect Italian brand in tubs. My favourite is Paesanella.
3. **Cannelloni** – The cannelloni tubes I use are the dried ones sold in boxes at supermarkets and delis. They are about 11 cm / 4″ long and 2.5cm / 1″ wide. They do not need to be boiled before cooking in the oven. 
This recipe can also be used for manicotti which are larger tubes with ridges – use 10 to 12.
You can also make this using fresh lasagna sheets. Just roll Filling up inside, place in the baking pan seam side down. I prefer using dried tubes – refer in post for the reason why.
4. **For FREEZING or refrigerating uncooked**: Add extra 3/4 cup water* into Sauce, cook Sauce per recipe then blend* until smooth. Place filled, uncooked cannelloni in single or multiple dishes, cover with sauce (will look like LOTS!) then cheese. Cover, cool then freeze / refrigerate.
On the day of, thaw then bake per recipe (covered then uncovered). OR bake from frozen, bake covered 35 minutes then uncovered 10 minutes.
1. The reason for extra water and blending: When uncooked cannelloni is assembled, the dry pasta will absorb some of the liquid as it freezes. If you don’t add extra water, the sauce dries up once baked. Blending also helps here, plus it brings the sauce together better so it doesn’t split when thawed.
COOKED leftovers – refrigerate up to 3 days or freeze, thaw, then reheat covered in microwave for best results.


source: https://www.recipetineats.com/spinach-ricotta-cannelloni/#wprm-recipe-container-23117