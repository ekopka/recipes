# Vegan pasta salad with green goddess dressing

## Ingredients

- 1/2 (about 650g) butternut pumpkin, peeled, deseeded, cut into 2cm pieces
- 2 (about 260g) zucchini, cut into 2cm pieces
- 250g baby roma tomatoes, halved
- 250g dried fusilli pasta
- 1 small red onion, thinly sliced
- 2 tablespoons pine nuts, toasted
- Baby rocket leaves, to serve


### Dressing

- 1 ripe avocado, chopped
- 1 bunch fresh continental parsley, leaves picked
- 1 bunch fresh coriander, leaves picked
- 2 garlic cloves, chopped
- 60ml (1/4 cup) fresh lemon juice
- 2 tablespoons extra virgin olive oil


## Method

### Step 1

Preheat the oven to 200C/180C fan forced and line 2 baking trays with baking paper. Arrange the pumpkin and zucchini on 1 prepared tray and the tomatoes on the other prepared tray. Spray with olive oil. Roast the pumpkin and zucchini for 15 minutes. Add the tomatoes to the oven and roast for 30 minutes or until the vegies are tender and lightly browned.

### Step 2

Meanwhile, cook the pasta in a large saucepan of boiling salted water following packet directions or until al dente. Drain and rinse well under cold running water. Drain well.

### Step 3

To make the dressing, combine all the ingredients in a small food processor and process until smooth. Season.

### Step 4

Toss pasta with the dressing, then fold through the pumpkin, zucchini, tomato, and onion. Sprinkle with pine nuts and scatter with rocket leaves.
