# Vegan Potato Salad

## Ingredients

- 2 1/2 lbs. yukon gold potatoes (red or white is great too)
- 1/2  cup celery (some leaves ok), diced (optional)
- 1/2 cup green onions (green parts only), diced
- 1/4 – 1/3 cup dill or sweet relish or finely chopped pickles, optional


### Potato Salad Dressing

- 1/2 – 3/4 cup vegan mayo (store bought or homemade vegan mayo)
- 1 heaping tablespoon yellow mustard
- 2 teaspoons apple cider vinegar or juice of 1/2 lemon, optional
- 1/4 teaspoon ground celery seed
- mineral salt & fresh cracked pepper, to taste


## Instructions

**Potatoes**: Place potatoes in a large pot, cover potatoes with 1 1/2 inch of water, add generous pinch of salt. Bring to a boil, reduce heat to a gentle boil and cook about 15 – 20 minutes, until potatoes are just fork tender. Drain potatoes and let cool. Once cooled, gently peel the skin away (or leave it on if you prefer). Pinch the potato skin with your thumb and forefinger, skin should come off with ease. Cut the potatoes into 1/2 inch cubes. Alternatively, cube the potatoes first, then boil.

**Prep veggies**: While potatoes are cooking and/or cooling, prepare the green onion, and celery.

**Dressing**: In a small bowl, combine the mayo, mustard, ground celery seed, salt and pepper, and mix well. If dressing is too thick, add a tad more water or lemon juice.  Alternatively, if dressing is too thin, add more mayo. If using store bought mayo, I like to add 2 tablespoons of water to thin, as store bought may is very thick. 

**Assemble**: In a large bowl (or pot you cooked the potatoes in) place the potatoes, onion and optional celery. Pour the dressing over top and mix to combine. Season with mineral salt and fresh cracked pepper.

Serve as is, or store in the refrigerator for an hour or so before serving. Garnish with paprika, chives, parsley, dill or even radishes.

Store leftovers in an airtight container in the refrigerator for up to 5 days.