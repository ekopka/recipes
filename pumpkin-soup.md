# Better Pumpkin Soup

## Ingredients

### Core Vegetables
- 1 onion, peeled and cut into 1 cm pieces
- 1 kg pumpkin, peeled and cut into 5 cm pieces
- 1 large potato, peeled and cut into 2 cm pieces
- 1 garlic clove , peeled and cut

### Liquid
- 1 ltr (4 cups) vegetable or chicken stock

### Spices
- 1/2 tspn corriander
- 1 tspn cumin
- 1/2 tspn nutmeg
- salt to taste

### To serve
- 1/2 cup cream

## Method

1. Prepare **Vegetables**
1. Cook **onion** and **garlic** in pot with a dash of oil
1. Add **pumpkin** and **potatoes** 
1. Add **stock** and **Spices**
1. Simmer ingrediants for 30 minutes
1. Blend in batches after cooling
1. Reheat and add **cream**
1. Serve hot with bread
