# Vegetable Fritters

## Ingredients

- 2 cups shredded zucchini
- 2 cups shredded carrots
- 2 cloves garlic, minced
- 2/3 cup all-purpose flour
- 2 large eggs, lightly beaten
- 1/3 cup sliced spring onions (green and white parts)
- 2 Tablespoons olive oil
- Sour cream or yogurt, for serving

## Method

1. Place the shredded zucchini in a colander and sprinkle it lightly with salt. Let the zucchini sit for 10 minutes then using your hands, squeeze out as much liquid as possible.
1. Transfer the zucchini to a large bowl then add the carrots, garlic, flour, eggs, spring onions, ¼ teaspoon salt and ⅛ teaspoon pepper. Stir the mixture until it is combined.
1. Line a plate with paper towels. Place a large sauté pan over medium-high heat and add the olive oil. Once the oil is shimmering, scoop 3-tablespoon mounds of the vegetable mixture into the pan, flattening the mounds slightly and spacing them at least 1 inch apart.
1. Cook the fritters for 2 to 3 minutes then flip them once and continue cooking them an additional 1 to 2 minutes until they’re golden brown and crispy. Transfer the fritters to the paper towel-lined plate, season them with salt and repeat the cooking process with the remaining mixture.
1. Serve the fritters immediately topped with sour cream or yogurt.
