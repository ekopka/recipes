# Cashew Ricotta Cheese

Makes aprox. 2 cups

## Ingredients
- 1 1/2 cups raw cashews, soaked
- 1/2 cup water
- juice of 1 large lemon or 1 tablespoon apple cider vinegar
- 1 – 2 tablespoons nutritional yeast, optional
- 1 small garlic clove
- 1/2 teaspoon onion powder
- himalayan sea salt & cracked pepper, to taste


## Method

**Soak Cashews:** First, it’s best to soak your cashews in water before starting. There are 2 ways to soak:
- Soak cashew in cool water for at least 2 hours in a bowl of water, covering the cashews about 2 – 3 inches of water as they will swell up.
- For a quick method of soaking, let cashews sit in hot water for 5 minutes.

**Blend:** Drain cashews and place all remaining ingredients into a blender or food processor, process until creamy stopping to scrape down the sides every few minutes. Taste for flavors adding any additional ingredients. Some like a salty ricotta so feel free to add as much salt as you want.

**Chill:** Store in an airtight container in the refrigerator for an hour or two, this will stiffen the mixture a bit. You can also prepare your dish straight away without refrigeration if needed. Refrigeration will thicken the mixture a bit. If it becomes too thick, add a tad of water and mix well.

**Store:** Leftovers can be stored in the refrigerator for up to a 5 – 6 days. To keep longer, store in the freezer for up to 2 months in a freezer safe container. Let thaw in the refrigerator. Give a good stir before using.
