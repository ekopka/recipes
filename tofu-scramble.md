# Tofu Scramble

- (1) 450 g block firm tofu
- 1 tablespoon olive oil
- 2 tablespoons nutritional yeast
- 1/2 teaspoon salt, or more to taste
- 1/4 teaspoon turmeric
- 1/4 teaspoon garlic powder
- 2 tablespoons non-dairy milk, unsweetened and unflavored

1. Mash 450 g block of firm tofu, crumble to desired consistency. Use potato masher, for, or hands.
1. Heat 1 tablespoon olive oil in a pan over medium heat.
1. Add tofu to pan and cook, stirring frequently, for 3-4 minutes until the water from the tofu is mostly gone.
1. Add 2 tbsp nutritional yeast, 1/2 tsp salt, 1/4 tsp turmeric powder, 1/4 tsp garlic powder.
1. Cook and stir constantly for about 5 minutes.
1. Add 2 tbsp milk.
1. Mix and serve.