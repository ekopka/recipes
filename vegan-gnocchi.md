### Creamy vegan sun-dried tomato and broccolini gnocchi

## Ingredients

- 1 garlic clove
- 1 bunch broccolini
- 500g pkt fresh mini or regular-sized gnocchi
- 150g (1 cup) frozen peas
- 100g sun-dried tomato strips, 1 tbs oil reserved from jar
- 1 tbs plain flour
- 375ml (11 ⁄2 cups) So Good Almond Original milk
- Fresh basil leaves, to serve

## Method

### Step 1

Put the kettle on.

### Step 2

While the kettle boils, crush the garlic and cut the broccolini stalks in half lengthways. Heat a deep frying pan over medium-high heat.

### Step 3

Pour the boiling water into a large saucepan over high heat. (Don’t fill too high or it will take too long to boil again.) Add the gnocchi and cook until gnocchi rise to the surface, adding the peas and broccolini in the last minute of cooking. Drain.

### Step 4

Once the frying pan is hot, pour in the reserved sun-dried tomato oil. Add the garlic and cook, stirring, for 30 seconds. Add the flour. Cook, stirring, for 30 seconds. Remove from heat and gradually whisk in the almond milk until well combined. Return to a medium heat and cook, stirring constantly, until the mixture boils. Simmer for 3 minutes or until the sauce thickens slightly. Season.

### Step 5

Add the gnocchi mixture and tomato strips to the sauce and stir to combine. Divide among serving bowls, season and top with basil.
