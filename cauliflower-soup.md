# Cauliflower Soup

## Ingredients

- 1 tablespoon olive oil
- 1 large brown onion, chopped
- 2 garlic cloves, crushed
- 1 head (1.3kg) cauliflower, cut into florets
- 500g sebago potatoes, peeled, chopped
- 1 litre Massel chicken style liquid stock (see notes)
- 1/2 cup pure cream


## Method

Step 1 

Heat oil in a large saucepan over medium-high heat. Add onion and garlic. Cook, stirring, for 3 minutes or until onion has softened. Add cauliflower and potato. Cook, stirring, for 5 minutes. 

Step 2 

Add stock. Season with pepper. Cover. Bring to the boil. Reduce heat to medium-low. Simmer for 15 to 20 minutes or until potato is tender. Set aside for 5 minutes to cool slightly. 

Step 3 

Blend, in batches, until smooth. Return to pan over low heat. Add pure cream. Cook, stirring, for 2 minutes or until heated through. Serve. 
