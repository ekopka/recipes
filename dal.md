# Dal (Somya's)

## Ingredients
- 1 cup red lentils
- 2 fresh tomatoes
- 1 onion
- 3 cloves garlic
- slither of garlic
- 4-5 cups water
- 1 tsp coriander powder
- 1 tsp red chilli powder
- 1 tsp turmeric powder
- salt to taste
- 2-3 tsp ghee


## Method
1. Take one cup of red lentils, wash, soak and set aside.
1. Roughly chop two fresh tomatoes, one onion, three cloves of garlic, and a slither of garlic.
1. Get a deep pan/wok/ any utensil that is deep enough.
1. Put your daal and all the chopped veggies in the pan.
1. Put 4-5 cups of water in the pan
1. Put a teaspoon of coriander powder, teaspoon of red chilli powder, teaspoon of turmeric powder, and salt to taste in the pan
1. Put two-three teaspoons of ghee
1. Turn on the flame and let it cook on medium to low, with a lid, for about 20-30 mins stirring in between
1. Optional (be extremely careful if you choose to do this)- Take a small microwave proof bowl and put two teaspoons of ghee, one teaspoon of cumin seeds, put the bowl in microwave for 45-50 seconds. Carefully take out the bowl (wear mittens), then carefully pour on top of dal when its done. 
1. Garnish (optional) - finely chop some fresh coriander leaves and put in dal after its done

