# Cashew Sour Cream

## Ingredients

- 1 cup raw cashews
- 1/2 cup water
- 1 tablespoon lemon juice
- 3/4 teaspoon apple cider vinegar
- generous pinch of mineral salt

## Method

Soak the cashew in 1 of 2 ways, then drain and rinse well.

1. Soak cashews covered with 2 – 3 inches of water for 2 – 3 hours, overnight is great too but not necessary for cashews.
1. For a faster soak, add them to hot water, not boiling, and let soak for 5 – 10 minutes.

In high-speed or personal blender, add all ingredients and blend until nice and creamy, stopping to scrap down the sides every now and then. Add a tad more water as needed to create desired consistency.

Sour cream is best refrigerated for at least an hour before serving, but will do fine if served right away. Sour cream will thicken upon being chilled.

Makes about 1 ¼ cup.  Will keep in refrigerator for 5 – 6 days.
