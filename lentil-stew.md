# Lentil Stew

## Ingredients

- 1 cup red lentils, dried
- 1 cup rice
- 4 cloves garlic
- 1 medium onion
- 1 tbsp tomato paste
- 2 cups vegetable broth
- 4-5 cups water
- 1 tsp cumin
- 1 tsp turmeric
- 1½ tsp sweet paprika (smoked paprika would also work nicely)
- ¼ tsp cayenne pepper
- 1 bay leaf
- 1 tbsp thyme, dried
- 5 tbsp olive oil
- salt to taste

## Method

Toast in pot for 2-3 minutes (until smell):
- 1 tsp cumin
- 0.25 tsp cayenne pepper

Dice:
- 4 cloves garlic
- 1 medium onion

Add to pot:
- 5 tbsp olive oil
- garlic
- onion

Cook for 5 minutes, stirring occasionally.

Add to pot:
- 2 cups vegetable broth
- 4-5 cups water
- 1 cup red lentils, dried
- 1 cup rice
- 1 tbsp tomato paste

When boils add:
- 1 tsp turmeric
- 1 bay leaf
- 1 tbsp thyme, dried

Simmer for 20-25 minutes, stirring every few minutes, until rice cookes.

Add:
- salt to taste
- 1½ tsp sweet paprika (smoked paprika would also work nicely)

Mix and serve.