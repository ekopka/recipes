# Turkish Bread

1. Combine plain flour (500g, 3 1/3 cups), yeast (1 teaspoon dried), sugar (1 teaspoon sugar) and salt (1 teaspoon salt) in a bowl.
1. Make a well in the centre, then add warm water (175 mL 1 1/2 cups).
Stir until combined.
1. Use hands to bring the dough together in the bowl.

Turn the dough onto a lightly floured surface and knead for 15 minutes or until smooth and elastic. Brush a bowl with oil to grease. Place dough in the bowl and lightly coat with oil. Cover with a damp tea towel. Set aside in a warm, draught-free place for 1-1 1/2 hours or until the dough has doubled in size. 

Place a baking tray on the middle shelf of the oven. Preheat oven to 230°C. Turn dough onto a lightly floured surface and cut in half. Flatten slightly with hands. Place each half on separate pieces of floured, non-stick baking paper. Cover with a damp tea towel and set aside in a warm, draught-free place for 15 minutes.

With floured hands, stretch each piece of dough into an 18 x 40cm rectangle. Leave on non-stick baking paper. Cover with a damp tea towel and set aside in a warm, draught-free place for 10 minutes. Combine sesame (2 tsp) and nigella (` tsp) seeds. 

Whisk egg yolk (1) and oil (1 tbsp) in a bowl. Brush the top of each pide with egg mixture. Use floured fingers to make indentations on top and sprinkle with seeds. Remove tray from oven and slide 1 pide onto tray. Cook for 8-10 minutes or until golden. Cool on a wire rack. Repeat with second pide. 
