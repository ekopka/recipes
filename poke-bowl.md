# Vegan Tofu Poke Bowl

## Ingredients

### Tofu Poke

- 1/4 to 1/3 cup tamari, coconut aminos or soy sauce
- 1 tablespoon rice vinegar or lime juice
- 1/2 tablespoon sambal oelek
- 1 teaspoon sesame oil
- 2 cloves garlic, smashed and finely chopped or finely grated
- 1 inch piece ginger, peeled and finely chopped or grated
- 1/3 sweet onion, such as Maui onion, julienned
- 0.4 kg block organic tofu (firm or extra firm), cut into 1/2 inch cubes


### Bowl Filling

- 1/2 cucumber, sliced
- 3 – 4 radishes, sliced
- 1/4 red cabbage, shredded
- 1 avocado, diced or sliced
- 1 tablespoon sesame seeds, toasted or black
- 2 scallions (shallots), white and green parts, sliced thinly on the bias
- lime wedges
- 1/3 cup cilantro (coriander), roughly chopped
- Dynamite Sauce, optional
- 1 1/2 cups cooked rice (brown, black or cilantro lime rice) or quinoa


## Instructions

Drain the tofu and place it on its side, cut in half down the long side. Place on a clean dish cloth or between paper towels and gently press to soak up some of the moisture. Dice the tofu into 1/2 inch cubes.

In a medium bowl or shallow dish, combine the soy sauce, sambal, sesame oil, garlic, ginger and sweet onions. Toss in the tofu and let rest in for 10 minutes.

Prep the remaining ingredients and assemble your bowls.

Serves  3

Notes

### For variation try adding

- sliced carrots (or carrot ribbons)
- diced mango
- or cubed pineapple

